// console.log("Heeelloooo!");

// [SECTION] OBJECTS
	// An object is a data type that is used to represenrt world objects.
	// it is a collection of related data and/or functionalities or method.
	// information stored in objects are represented in "key:value" pair.
	// different data types may be stored in an object's property creating data structures.


// Creating Objects using Object Initializer/Object Literal Notation
/*
	SYNTAX:
		let/const objectName = {
			keyA: valueA,
			keyB: valueB,
			. . .
		}

		- this creates/declares an object and also initializes/assigns its properties upon creation.
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999,
	features: ["send message","make call","play games"],
	isWorking: true,
	owner: {
		fullName: "Chris",
		yearOfOwnage: 20,
	}
}
console.log(cellphone);


// Accessing/reading properties
	
	// Dot Notation
		/*
			SYNTAX:
				objectName.propertyName;
		*/

console.log("Accessing using Dot Notation:")
console.log(cellphone.name); //returned value, Chris
console.log(cellphone.isWorking); // true
console.log(cellphone.features); // features array
console.log(cellphone.features[1]); // features array index 1

console.log(cellphone.owner.fullName); // Chris
// console.log(cellphone.owner); // error. owner is an object

	// Bracket Notation

console.log("Accessing using Bracket Notation:")
console.log(cellphone["name"]);
console.log(cellphone["owner"]["fullName"]);


// Creating Objects using Constructor Function
/*
	- creates reusable function to create several objects that have the same data structure
	 - this is useful for creating multiple instances/copies of an object.
	 - "INSTANCE" is a concrate occurence of any object which emphasizes distinct/unique identity

	 SYNTAX:
	 	function objectName(valueA,valueB){
			this.propertyA = valueA,
			this.propertyB = valueB
	 	}
*/

	// This is a constructor Function
		function Laptop(name, manufactureDate,ram,isWorking){
			// this keyword allows us to assign object properties by associating them with the values received from a consructor function's parameter.
			this.laptopName = name,
			this.laptopManufactureDate = manufactureDate,
			this.laptopram = ram,
			this.isWorking = isWorking
		}

		// Instatiation
			// the "new" keyword creates an instance of an object
			// not adding 'new' will result to 'undefined'
		let laptop = new Laptop("Lenovo","2008","2 gb",true)
		console.log(laptop);

		let myLaptop = new Laptop("Macbook Air","2020","8 gb",false)
		console.log(myLaptop);

		let oldLaptop = new Laptop("Portal R2E CCMC","1980","500 mb",false)
		console.log(oldLaptop);

		function Menu(mealName,mealPrice){
			this.mealName = mealName,
			this.mealPrice = mealPrice
		}

		let mealOne = new Menu("Breakfast", 299);
		console.log(mealOne);

		let mealTwo = new Menu("Dinner", 500);
		console.log(mealTwo);

		// Create empty object
		let computer = {};
		let emptyObject = new Object();
		console.log(computer);
		console.log(emptyObject);

		// Access objects inside an array
		let array = [laptop, myLaptop];
		console.log(array);

		// Get the value of the property laptopName in the first element of our object. // nested objects
		console.log(array[0].laptopName);
		console.log(array[1].laptopManufactureDate);


// [SECTION] Initialization / Adding / Deleting / Reassigning Object Properties
/*
	- like any other variable in JavaScript, objects have their properties initialized or added after the object was created/declared
*/

let car = {};
console.log(car);
	

	// Initialize/ Add object property using Dot Notation
	/*
		SYNTAX:
			objectName.propertyTOBeAdded = value;
	*/
	car.name = "Honda Civic";
	console.log(car);


	// Initialize / Add object property using Bracket Notation
	/*
		SYNTAX:
			objectName[propertyTOBeAdded] = value;
	*/
	car["manufactureDate"] = 2019;
	console.log(car);


	// Deleting Object Porperties using Dot Notation
	/*
		SYNTAX:
			delete objectName.propertyToBeDeleted;
	*/
	delete car.name;
	console.log(car);


	// Deleting Object Porperties using Dot Notation
	/*
		SYNTAX:
			delete objectName[propertyToBeDeleted];
	*/
	delete car["manufactureDate"];
	console.log(car);


	// Reassigning Object Properties
	car.name = "Honda Civic";
	car["manufactureDate"] = 2019;
	console.log(car);

		// Reassigning object property using Dot Notation
			car.name = "Dodge Charger R/T"
			console.log(car);

		// Reassigning object property using Bracket Notation
			car[name] = "Jeepney"
			console.log(car);

			// REMINDER: IF you will add a property to the object, make sure that the property name that you are going to add is not existing or else it will just reassign the value of the property.


// [SECTION] OBJECT METHODS
/*
	A "METHOD" is a function which is a property of an object

	THey are also functions and one of the key differences they have is that methods are functions related to a specific object.

*/
	let person = {
		name: "John Doe",
		talk: function(){
			console.log("Hello! My name is " + this.name)
		}
	}
	console.log(person);
	person.talk();

	// add method to object
		person.walk = function(){
			console.log(this.name + " walked 25 steps forward.")
		}
		console.log(person);
		person.walk();

		// methods are useful for creating reusable functions that perform tasks related to objects


	// constructor function with method
		function Pokemon(name, level){
			this.pokemonName = name;
			this.pokemonLevel = level;
			this.pokemonHealth =  2 * level;
			this.pokemonAttack = level;

		// METHOD
			// tackle
			this.tackle = function(targetPokemon){

				console.log(this.pokemonName + "tackles " + targetPokemon.pokemonName)

				let hpAfterTackle = targetPokemon.pokemonHealth - this.pokemonAttack;

				console.log(targetPokemon.pokemonName + " health is now reduced to " + hpAfterTackle)
			}
			// faint
			this.faint = function(){
				console.log(this.pokemonName + " fainted!")
			}
		}

		// Create Pokemon
			let pikachu = new Pokemon("Pikachu", 12);
			console.log(pikachu);
			let gyarados = new Pokemon("Gyarados", 20);
			console.log(gyarados);

		pikachu.tackle(gyarados);
		gyarados.tackle(pikachu);

		pikachu.faint();

	